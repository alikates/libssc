# libssc

Library to expose Qualcomm Sensor Core sensors.

## License

Available under the GPLv3 license
Copyright (c) by Dylan Van Assche (2022-2023)
